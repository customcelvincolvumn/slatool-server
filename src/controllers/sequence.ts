import { Request, Response } from 'express';
import db from '../config/lib/db';

export const createSequence = function (req: Request, res: Response) {
  const data = {
    project_json: req.body.project_json,
    user_idx: req.body.user_idx
  };

  db.query(
    'INSERT INTO squence_data SET ?',
    data,
    function (err, rows, fields) {
      if (err) {
        console.log(err);
        throw err;
      }

      // redirect to index page
      res.redirect('/');
    }
  );
};
